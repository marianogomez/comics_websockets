<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MyApp;

/**
 * Description of Comic
 *
 * @author mariano.gomez
 */
class Comic {
	
	public static final function getNoticias($request) {
		$response = Comic::dbGetNews($request);
		return $response;
	}
	
	public static final function getComics($request, $isLogged) {
		if(!$isLogged) {
			$response = new \stdClass();
			$response->code = "004";
			$response->type = "ERROR";
			$response->message = "Usuario no logueado";
		} else {
			$response = Comic::dbGetComics($request->criterio);
		}
		return $response;
	}
	
	public static final function dbGetComics($criterio){
		$dsn = 'mysql:dbname=curso.fullstack;host=localhost';
		$myPDO = new \PDO($dsn, 'root', '');
		$criterio = explode(' ', $criterio);
		
		$statement = $myPDO->prepare('SELECT * FROM comic_'.$criterio[0]);
		$params = array(
		);
		
		
		if (!$statement->execute($params)) {
			$response = new \stdClass();
			$response->code = "001";
			$response->type = "ERROR";
			$response->message = "Problema con la DB";
			return $response;
		}
		
		$resultadosCriterio = $statement->fetchAll();
		
		$response = new \stdClass();
		$response->resultados_criterio = array();
		$response->criterio = $criterio[0];
		foreach($resultadosCriterio as $resultado) {
			$indexArray[] = $resultado["id"];
			$response->resultados_criterio[] = $resultado;
		}
		
		$criterioIds = implode(',', $indexArray);
		
		$statement = $myPDO->prepare('SELECT * FROM comic WHERE id_'. $criterio[0] .' in ('.$criterioIds.')') ;
		
		if (!$statement->execute($params)) {
			$response = new \stdClass();
			$response->code = "001";
			$response->type = "ERROR";
			$response->message = "Problema con la DB";
			return $response;
		}
		
		$response->code = "103";
		$response->type = "SUCCESS";
		$response->comics = $statement->fetchAll();
		return $response;
	}
	
	public static final function dbGetNews($request){
		$dsn = 'mysql:dbname=curso.fullstack;host=localhost';
		$myPDO = new \PDO($dsn, 'root', '');
		
		$fromRow =  isset($request->indice_desde) ? strval(intval($request->indice_desde)) : "0";
		
		$statement = $myPDO->prepare('SELECT * FROM news LIMIT '.$fromRow.', 2');
		$params = array(
		);
		
		if (!$statement->execute($params)) {
			$response = new \stdClass();
			$response->code = "001";
			$response->type = "ERROR";
			$response->message = "Problema con la DB";
			return $response;
		}
		
		$noticias = 
		
		$response = new \stdClass();
		$response->noticias = $statement->fetchAll();
		
		$response->code = "104";
		$response->type = "SUCCESS";
		return $response;
	}
}
