<?php

namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use MyApp\User;

class ComicsApp implements MessageComponentInterface {
	protected $clients;

	public $logueados;
	
    public function __construct() {
        $this->clients = new \SplObjectStorage;
		$this->logueados = array();
    }
	
    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n\n\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        //$from->send("respuesta al que envio el msg");
		# si el msg empieza con login:
		$request = json_decode($msg);
		if(!isset($request->request)) {
			$response = $this->generateErrorResponse('000', 'ERROR', 'mensaje adulterado, intentar nuevamente');
		}
		switch ($request->request) {
			case 'user.login':
					$response = User::existsUser($request );
					if($response->code == "101") {
						$this->logueados[$from->resourceId] = true;
					}
					file_put_contents('archivo.txt', var_export($this->logueados, true));

					break;
//			case 'user.profile':
//					$response = User::existsUser($request, $this->logueados, $from->resourceId );
//					break;
			case 'comic':
					$response = Comic::getComics($request, $this->logueados, $from->resourceId );
					break;
			case 'comic.news':
					$response = Comic::getNoticias($request );
					break;
			
		}
		$from->send(json_encode($response));
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
		unset($this->logueados[$conn->resourceId]);
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n\n\n\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
		$this->clients->detach($conn);
    }

	private final function generateErrorResponse($errorCode, $errorType, $errorMsg){
		$response = new \stdClass();
		$response->code = $errorCode;
		$response->type = $errorType;
		$response->message = $errorMsg;
		return $response;
	}
}