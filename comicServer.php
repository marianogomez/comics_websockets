<?php
require dirname(__DIR__) . '/web.sockets/vendor/autoload.php';

//use Ratchet\Server\IoServer;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyApp\ComicsApp;

/*
$server = IoServer::factory(
	new Chat(),
	8080
);*/

$server = IoServer::factory(
	new HttpServer(
		new WsServer(
			new ComicsApp()
		)
	),
	8080
);

$server->run();
