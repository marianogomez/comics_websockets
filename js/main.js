var websocket;
var newsLastIndex = 0;
var noMoreNews = false;

//	I define the templates
var renderLoginTemplate;

//	I define the logic variables/objects
var logged = false;

var datos = {
	usuario: 'Función 13'
};

function init() {
	renderLoginTemplate = Handlebars.compile($('#hbt-login-box').html());
}

function iniciarSlide() {
	//	Inicializando slide
	$("#un-slides").unslider({
		animation: 'fade', 
		autoplay: true, 
		arrows: false
	});
	
}

function agregarEventosLogin(){
	$('.js-login-modal-triggers a').on('click', function (e) {
		e.preventDefault();
		$('#login-modal').modal('toggle');
	});
		
	$('#login-form').on('click', 'button', function(e) {
		$form = $(this).closest('form');
		$form.find('.alert').addClass('hide');
		e.preventDefault();
		username = $form.find('#login-user');
		password = $form.find('#login-pass');
		if(username.val() == "" || password.val() == "" ) {
			$form.find('.alert').text("Todos los campos son obligatorios").removeClass('hide');
		} else {
			params = {
				request: "user.login",
				username: username.val(),
				password: password.val(),
			}
			websocket.send(JSON.stringify(params));
		}
	});
}

function agregarEventosTabs() {
	$('#generos-tab').on('click', function(){
		params = {
			request: "comic",
			criterio: "genero",
		}
		websocket.send(JSON.stringify(params));
	});
	$('#ediciones-tab').on('click', function(){
		params = {
			request: "comic",
			criterio: "edicion",
		}
		websocket.send(JSON.stringify(params));
	});
	$('#personajes-tab').on('click', function(){
		params = {
			request: "comic",
			criterio: "superheroe",
		}
		websocket.send(JSON.stringify(params));
	});
	
	$('#home').on('click', '.link-mas-noticias', function(e){
		e.preventDefault();
		if(!noMoreNews) {
			params = {
				request: "comic.news",
				indice_desde: newsLastIndex,
			}
			websocket.send(JSON.stringify(params));
		}
	})
}

//	en login defino el comportamiento para cuando llegan respuestas vinculadas al login
var user = {
	loginSuccess: function(response) {
		$('#login-modal').modal('toggle');
		$('.js-login-modal-triggers').html("Bienvenido <b>" + response.user.username + "</b>!");
		$('#navbar').append('<li class=""><a href="#user-profile" data-toggle="tab">Mi perfil</a></li>');
		$('#content-container').append('<div  class="tab-pane" id="user-profile">'+
				'<div class="form-group">'+
					'<h3>Nombre de usuario:</h3><h5>'+ response.user.username + '</h5><br/>'+
					'<h3>Nombre:</h3><h5>'+ response.user.nombre + '</h5><br/>'+
					'<h3>Apellido:</h3><h5>'+ response.user.apellido + '</h5><br/>'+
					'<h3>Domicilio:</h3><h5>'+ response.user.domicilio + '</h5><br/>'+
					'<h3>Email:</h3><h5>'+ response.user.email + '</h5><br/>'+
				'</div>'+
			'</div>');
		$('#navbar .active').click();
	},
	
	loginFail: function(response) {
		$('#login-form').find('.alert').html('El nombre de usuario y/o la contraseña ingresada no existe o no es válido/a').removeClass('hide');
	},
};

var comic = {
	notLogged: function(response){
		obj = $('#content-container .active');
		console.log(obj.attr('id'));
		obj.html('<div class="alert alert-danger">Para ver esta información debe estar logueado</div>');
	},
	notNews: function(response){
		obj = $('#content-container .active');
		obj.html('<div class="alert alert-warning">Para ver esta información debe estar logueado</div>');
	},
	comicsSuccess: function(response){
		obj = $('#content-container .active');
		this.populateTabWithContent(obj, response);
	},
	noticiasSuccess: function(response){
		//obj = $('#content-container .active').children().last();
		obj = $('#content-container .active');
		obj.find('.link-mas-noticias').remove();
		if(response.noticias.length) {
			for (indice in response.noticias) {
				$(obj).append(
					'<div class="row article">'+
						'<div class="col-md-2">'+
							'<img src="images/news/'+response.noticias[indice].id+'.jpg" class="responsive" style="max-width: 150px;"/>'+
						'</div>'+
						'<div class="col-md-10">'+
							'<h4>'+response.noticias[indice].titulo+'</h4>'+
							response.noticias[indice].cuerpo+
						'</div>'+
					'</div>');
			}
			$(obj).append('<a class="link-mas-noticias" href="">Ver mas noticias</a>');
		} else {
			$(obj).append('No hay mas noticias para ver!</a>');
			noMoreNews = true;
		}
		newsLastIndex += response.noticias.length;
	},
	
	populateTabWithContent: function(tab, response) {
		selectElement = $('<select id="filtro-'+response.criterio+'" data-criterio="'+response.criterio+'" class="form-control"></select>');
		selectElement.append('<option value="0">Todos</option>');
		for(indice in response.resultados_criterio) {
			item = response.resultados_criterio[indice];
			selectElement.append('<option value="'+item.id+'">'+item.nombre+'</option>');
		}
		tab.html(selectElement);
		tab.find('#filtro-'+response.criterio).on('change', function(){
			value = $(this).val();
			criterio = $(this).data('criterio');
			if(value!=0) {
				$(this).closest('.tab-pane').find('.comic').addClass('hide').end().find('[data-'+criterio+'="'+value+'"]').removeClass('hide');
			} else {
				$(this).closest('.tab-pane').find('.comic').removeClass('hide');
			}
			//console.log("buscar todos los comics que tengan en data-<criterio> el valor buscado, y esconder al resto");
		});
		
		//	agregar el listado de comics
		listElement = $('<div class="row"></div>');
		for(indice in response.comics) {
			item = response.comics[indice];
			listElement.append(
					'<div class="comic col-md-3 col-sm-6" data-genero="'+item.id_genero+'" data-edicion="'+item.id_edicion+'" data-superheroe="'+item.id_superheroe+'">'+
						'<div class="panel-heading">'+
							'<img class="responsive" style="max-width:150px;" src="images/comics/'+item.id+'.jpg"/>'+
						'</div>'+
						'<div class="panel-body">'+
							item.titulo + 
						'</div>'+
					'</div>'
				);
		}
		tab.append(listElement);
	},
};

$(document).ready(function () {
	websocket = new WebSocket('ws://localhost:8080');
	
	websocket.onopen = function(evt) { 
		console.log("Connection open …"); 
		websocket.send(JSON.stringify({
			request: "comic.news",
			indice_desde: newsLastIndex,
		}));
	};

	websocket.onmessage = function(evt) { 
		response = JSON.parse(evt.data);
		switch (response.code){
			//	Login
			case '001':		
			case '002':		user.loginFail(response);	break;
			case '004':		comic.notLogged(response);	break;
			case '005':		comic.notNews(response);	break;
			
			case '101':		user.loginSuccess(response);	break;
			case '102':		user.myprofile(response);	break;
			case '103':		comic.comicsSuccess(response);	break;
			case '104':		comic.noticiasSuccess(response);	break;
			//	pedido de info
		}
		console.log(response);
	};

	websocket.onclose = function(evt) { 
		console.log("Connection closed 1."); 
		$('#error-modal').modal('show');
	};

	websocket.onerror = function(evt) { 
		console.log("Connection closed 2.");
		$('#error-modal').modal('show');
		
	};
	iniciarSlide();
	agregarEventosLogin();
	agregarEventosTabs();
	
	
	$('#disclaimer').on('click', function(){
		$('#disclaimer-modal').modal('toggle');
	});
});